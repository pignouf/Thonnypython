from ressources.thonnyPython import *


initExoDraw("Epreuve", 6,6, 0, 0, 0, "yellow")
exercice_dessin("Epreuve")
# tourner(90)
# 
def triangle(n, R, G, B):
    couleur(R, G, B)
    for i in range(3):
        avancer(n)
        tourner(180+60)
        

def polygone(nb_cote, taille_cote, x, y):
    # Aller au point de départ
    aller(x,y)
    # Dessiner le polygone
    for i in range(nb_cote):
        avancer(taille_cote)
        tourner(180+(180-(360//nb_cote)))      
    # Revenir à 0,0
    aller(0,0)

polygone(5, 2,1,0)
polygone(3, 3,3,3)
# triangle(4, 255, 0, 0)