import io 
import sys
import string

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    
def compare(s1, s2):
     remove =  string.whitespace #string.punctuation +
     return s1.translate(s1.maketrans('', '', remove)) == s2.translate(s2.maketrans('', '', remove))


old_stdout = new_stdout = sys.stdout
correctStr = None

def initExoPrint(exoName, _correctStr):
    global old_stdout, new_stdout, correctStr
    correctStr = _correctStr
    
def exercice_print(exoName):
    global old_stdout, new_stdout, correctStr
    old_stdout = sys.stdout    
    new_stdout = io.StringIO()
    sys.stdout = new_stdout


def fin_print():
    global old_stdout, new_stdout, correctStr
    output = new_stdout.getvalue()
    sys.stdout = old_stdout
    print(output)
    if(compare(correctStr, output)):
        print(bcolors.OKGREEN + bcolors.BOLD + "Bravo !" + bcolors.ENDC)
    else:        
        print(bcolors.FAIL + bcolors.BOLD + "Essaie encore" + bcolors.ENDC)
