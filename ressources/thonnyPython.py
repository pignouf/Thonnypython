from ressources.robot import initExoRobot, exercice_robot, fin_robot, gauche, droite, haut, bas, murDroite, murBas, murGauche, murHaut
from ressources.draw import initExoDraw, exercice_dessin, fin_dessin, avancer, tourner, couleur, lever, poser, aller
from ressources.print2 import initExoPrint, exercice_print, fin_print
from enum import Enum


class GameType(Enum):
    PRINT = 1
    ROBOT = 2
    DRAW = 3
    NOTFOUND = 4
    
__selectedGame = GameType.NOTFOUND
        

def load_exercice(exercice):
    global __selectedGame
    if exercice == "B-1":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice, "Bonjour"*7)
        
    elif exercice == "B-2":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice, "Coucou!"*100)
        
    elif exercice == "B-3":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice, "pingpong"*4)
        
    elif exercice == "B-4":
        __selectedGame = GameType.ROBOT
        initExoRobot(exercice, 9, 5, [[3,4,5],[0,1,5,6,7],[1,2,3,7,8],[3,4,5],[5,6,7]], [3,6], [0,0])
        
    elif exercice == "B-5":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 5, 4, 1, 0, -90, "green")
        
    elif exercice == "B-6":
        __selectedGame = GameType.ROBOT
        initExoRobot(exercice, 7,4, [[3,4,5,6],[1,5,6],[1,2,3],[1,2,3,4,5]], [3,0], [3,6])
        
    elif exercice == "B-7":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 3, 5, 2, 1, -90, "cyan")

    elif exercice == "B-8":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice, "ping"+"pong"*6+"ping")

    elif exercice == "B-9":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice, "ping"*3+"pong"*5)
        
    elif exercice == "B-10":
        __selectedGame = GameType.ROBOT
        initExoRobot(exercice, 9, 5, [[1,2,4,7,8],[2,3,4,5,7,8],[0,3,4,5,8],[0,1,4,5,6,8],[1,2]], [0,5], [0,0])
        
    elif exercice == "B-11":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 5, 6, 2, 0, -90, "yellow")
        
    elif exercice == "B-12":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 6, 5, 1, 4, 0, "blue")
        
    elif exercice == "B-13":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 4, 6, 1, 0, -90, "yellow")
        
    elif exercice == "B-14":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 5, 6, 1, 3, 0, "blue")

    elif exercice == "B-15":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice, "AAAABBBAAAAAA")
        
    elif exercice == "B-16":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 10, 4, 0, 3, 0, "white")

    elif exercice == "B-17":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice, ("XOXOXOXO"+"OXOXOXOX")*4)
        
    elif exercice == "B-18":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 10, 9, 5, 4, -90, "yellow")
        
    elif exercice == "D-1":
        __selectedGame = GameType.ROBOT
        initExoRobot(exercice, 12, 5, [[1,2,5],[1,5],[1,5,6,7,8,9],[3,9,10,11],[0,1,2,3,4,5,6,7]], [2,0], [4,11])
        
    elif exercice == "D-2":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 9, 5, 1, 4, 0, "green")
        
    elif exercice == "D-3":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 6, 8, 5, 3, 90, "blue")
        
    elif exercice == "D-4":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice, "0 2 4 0 2 0 2 4 6 8 10")
        
    elif exercice == "D-5":
        __selectedGame = GameType.ROBOT
        initExoRobot(exercice, 7,7, [[3,4,5],[0,1,5],[1,2,3,5],[3,5],[3,5,6],[3],[3,4,5]], [6,6], [0,0])
        
    elif exercice == "D-6":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 11, 5, 0, 4, 0, "blue")
        
    elif exercice == "D-7":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "ping"*3+"pong"+"ping"*5+"pong"+"ping"*2)

    elif exercice == "D-8":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  str(7**2)+str(10**2)+str(5**2))

    elif exercice == "D-9":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  str(99*3))        

    elif exercice == "D-10":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  str(4*99+5))        

    elif exercice == "D-11":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  str(99*99))          

    elif exercice == "D-12":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  str(15*99 + 8*99))    

    elif exercice == "D-13":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  str(2022-99))

    elif exercice == "D-14":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "12345678910")

    elif exercice == "D-15":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  str(sum(range(11))))
        
    elif exercice == "D-16":
        __selectedGame = GameType.ROBOT
        initExoRobot(exercice, 12, 10, [[3,4,5,6,7,8,9,10],[3],[3,5,6,7,8,9,10,11],[3,6,7],[4,7],[3,4,5,7],[3,4,5,7],[0,1,2,3,7],[5,6,7],[0,1,2,3,4,5]], [0,11], [8,0])
        
    elif exercice == "D-17":
        __selectedGame = GameType.DRAW
        initExoDraw(exercice, 8,3, 1,2, 0, "blue")
                
    elif exercice == "D-18":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "Bonjour, Madame !")
                
    elif exercice == "D-19":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "Bienvenue, Julien Dupont !")
                
    elif exercice == "D-20":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "bla"*10)
                
    elif exercice == "E-1":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "eau bouillante")
                
    elif exercice == "E-2":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "Vous êtes majeur")
                
    elif exercice == "E-3":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "Vous avez de la fièvre")
                
    elif exercice == "E-4":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "Le nombre est impair")
                
    elif exercice == "E-5":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "105")
                
    elif exercice == "E-6":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "Le deuxième !")
                
    elif exercice == "E-7":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "Salade de fruits !")
                
    elif exercice == "E-8":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "M")
                
    elif exercice == "E-9":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "bla"*2)
        
    elif exercice == "E-10":
        __selectedGame = GameType.ROBOT
        initExoRobot(exercice, 11, 5, [[4],[0,7],[7],[4,9]], [4,10], [0,0])
        
    elif exercice == "E-11":
        __selectedGame = GameType.ROBOT
        initExoRobot(exercice, 5, 11, [[1,3],[],[3],[2,3],[0],[0,1],[1],[1,2],[4],[1,3,4],[1,3,4]], [9,2], [0,2])
        
    elif exercice == "E-12":
        __selectedGame = GameType.PRINT
        initExoPrint(exercice,  "Année non bissextile")    
        
def debut_exercice(exercice):
    load_exercice(exercice)
    if __selectedGame == GameType.ROBOT:
        exercice_robot(exercice)
    elif __selectedGame == GameType.DRAW:
        exercice_dessin(exercice)
    elif __selectedGame == GameType.PRINT:
        exercice_print(exercice)
        
def fin_exercice():
    if __selectedGame == GameType.ROBOT:
        fin_robot()
    elif __selectedGame == GameType.DRAW:
        fin_dessin()
    elif __selectedGame == GameType.PRINT:
        fin_print()
