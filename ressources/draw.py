import turtle
import time


############## Constants ##############
pauseLength = 0.5
LENGTH = 100  # each grid element will be LENGTH x LENGTH pixels

      
############## Objects ##############
nbCasesX= nbCasesY = x0 = y0 = h0 = penWidth = initialColor = t = s = None



############## Functions ##############

def grid():
    global t,s, pauseLength, LENGTH, nbCasesX, nbCasesY, x0, y0, h0, penWidth, initialColor
    t.penup()
    t.goto(-(nbCasesX)*LENGTH/2, -(nbCasesY)*LENGTH/2)
    t.pendown()
    sign = 1
    for k in range(2):
        for _ in range([nbCasesX, nbCasesY][1-k]):
            t.forward(LENGTH * [nbCasesX, nbCasesY][k])
            t.left(sign * 90)
            t.forward(LENGTH)
            t.left(sign * 90)
            sign = 0 - sign
        t.forward(LENGTH * [nbCasesX, nbCasesY][k])
        [t.right, t.left][nbCasesY % 2](90)
        sign = 0 - sign
        

def initExoDraw(exoName, _nbCasesX, _nbCasesY, _x0, _y0,_h0, _initialColor):
    global t,s, pauseLength, LENGTH, nbCasesX, nbCasesY, x0, y0, h0, penWidth, initialColor
    nbCasesX = _nbCasesX
    nbCasesY = _nbCasesY
    x0 = _x0
    y0 = _y0
    h0 = _h0
    initialColor = _initialColor

def init(exo="B-7"):
    # Constants
    global t,s, pauseLength, LENGTH, nbCasesX, nbCasesY, x0, y0, h0, penWidth, initialColor
    WIDTH = (nbCasesX+2)*LENGTH
    HEIGHT = (nbCasesY+2)*LENGTH
    
    # Setup
    turtle.title("Exercice "+exo)
    t = turtle.Turtle()
    turtle.setup(WIDTH,HEIGHT)
    s = turtle.Screen()
    s.bgcolor("black")
    s.tracer(False)
    
    # Grid
    penWidth = LENGTH//15
    t.pensize(penWidth//1.5)
    t.color("#421346")
    grid()
    
    # Pen
    t.shapesize(penWidth,penWidth,penWidth)
    t.pensize(penWidth)
    t.speed(10)
    s.colormode(255)
    
    # Placement
    t.penup()
    t.setheading(0)
    t.color(initialColor, "black")
    t.goto(-(nbCasesX)*LENGTH/2, -(nbCasesY)*LENGTH/2)
    t.forward(LENGTH*x0)
    t.right(-90)
    t.forward(LENGTH*y0)
    t.right(90)
    t.right(h0)
    t.pendown()
    
    
    s.tracer(True)
    time.sleep(pauseLength)

def avancer(n):
    for i in range(n):
        time.sleep(pauseLength/2)
        t.forward(LENGTH)
        time.sleep(pauseLength/2)

def tourner(n):
    steps = abs(n//10)
    pause = pauseLength//steps
    sign = n/abs(n)
    
    for i in range(steps):
        t.right(n/steps)
        time.sleep(pause)

def couleur(r, g, b):
    t.pencolor(r, g, b)

# def couleur(c):
#     t.pencolor(c)
    
def lever():
    time.sleep(pauseLength/2)
    t.penup()
    t.shapesize(penWidth*1.1,penWidth*1.1,penWidth*1.1)
    time.sleep(pauseLength/2)
    
def poser():
    time.sleep(pauseLength/2)
    t.pendown()  
    t.shapesize(penWidth,penWidth,penWidth)
    time.sleep(pauseLength/2)
    
def aller(x, y):
    lever()
    t.goto(-(nbCasesX)*LENGTH/2 +x*LENGTH, -(nbCasesY)*LENGTH/2 + y*LENGTH)
    poser()
    
def exercice_dessin(exoName):
    init(exoName)
    
def fin_dessin():
    global t,s
    s.exitonclick()
        
############## Program ##############
if __name__ == "__main__":
    init()
    fin_dessin()

